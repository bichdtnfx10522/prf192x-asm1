
// Function for button with name "Try it"
function notify() {

	alert("Chào mừng bạn đến với ứng dụng JavaScript đầu tiên");
	document.getElementById("next").style = 'display :block;';

}

//add Name
function addName() {

	var addname = prompt("Hãy nhập họ tên của bạn: ", "");
	if (addname != "" && addname != null) {
		document.getElementById("name").innerHTML = "Họ tên: " + addname;
	} else {
		var continueName = confirm("Bạn chưa nhập họ tên, có muốn nhập lại họ tên không?");
		if (continueName == true) {
			addName();
		} else {
			alert('Tạm thời lưu thành "Chưa có bản ghi" ');
			document.getElementById("name").innerHTML = "Họ tên: Chưa có bản ghi";
		}
	}
}

//add City
function addCity() {
	var addcity = prompt("Hãy nhập tên thành thị bạn đang sống ", "");
	if (addcity != "" && addcity != null) {
		document.getElementById("city").innerHTML = "Thành phố: " + addcity;
	} else {
		var continueCity = confirm("Bạn chưa nhập tên thành phố bạn đang sống, có muốn nhập lại không ?");
		if (continueCity == true) {
			addCity();
		} else {
			alert('Tạm thời lưu thành "Chưa có bản ghi" ');
			document.getElementById("city").innerHTML = "Thành phố : Chưa có bản ghi";
		}
	}
}

//add Phone number
function addMobile() {
	var addmobile = prompt("Hãy nhập số điện thoại của bạn: ", "");
	if (addmobile != "" && addmobile != null) {

		document.getElementById("mobile").innerHTML = "Số điện thoại: " + addmobile;
	} else {
		var continueCity = confirm("Bạn chưa nhập số điện thoại, có muốn nhập lại không ?");
		if (continueCity == true) {
			addMobile();
		} else {
			alert('Tạm thời lưu thành "Chưa có bản ghi" ');
			document.getElementById("mobile").innerHTML = "Só điện thoại : Chưa có bản ghi";
		}
	}
}

// Caculate age
function caculateAge(age) {

	var day = new Date();
	var _age = day.getFullYear() - age;
	return _age;
}

//add Age 
function addAge() {
	var addYear = prompt("Hãy nhập năm sinh của bạn", "");
	var d = new Date();
	if (addYear != "" && addYear != null) {
		if (addYear > d.getFullYear()) {
			var continueAge = confirm("Bạn nhập năm lớn hơn năm sinh của bạn, có muốn nhập lại không?");
			if (continueAge == true) {
				addAge();
			} else {
				alert('Tạm thời lưu thành "Chưa có bản ghi"');
				document.getElementById("age").innerHTML = "Tuổi : Chưa có bản ghi";
			}
		} else {
			var age = caculateAge(addYear);
			document.getElementById("age").innerHTML = "Tuổi: " + age;

		}
	} else {
		alert('Tạm thời lưu thành "Chưa có bản ghi" ');
		document.getElementById("age").innerHTML = "Năm sinh : Chưa có bản ghi";
	}
}

function addEmail() {
	var addEmail = prompt("Hãy nhập email của bạn", "");
	if (addEmail != "" && addEmail != null) {
		document.getElementById("email").innerHTML = "Email : " + addEmail;
	}

}
function addJob() {
	var addJob = prompt("Hãy nhập email của bạn", "");
	if (addJob != "" && addJob != null) {
		document.getElementById("job").innerHTML = "Job : " + addJob;

	}
}
//function for next Button
var next = 0;
function nextButton() {

	//var nextButtonObj[] = ["Họ tên của bạn","Thành phố bạn đang sống",
	//"Số điện thoại của bạn","Năm sinh của bạn"];

	switch (next) {
		case 0:
			addName();
			next += 1;
			break;
		case 1:
			addCity();
			next += 1;
			break;
		case 2:
			addMobile();
			next += 1;
			break;
		case 3:
			addAge();
			next += 1;
			break;
		case 4:
			addEmail();
			next += 1;
			break;
		case 5:
			addJob();
			break;
		default:
			var continueNextButton = confirm("Bạn có muốn thực hiện lại từ đầu ?");
			next = 0;
			if (continueNextButton == true) {
				nextButton();
			}

	}
}